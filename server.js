const passport = require("passport");
const express = require("express")
const googleStratergy = require("./googleStrategy")
const app = express();
app.use(passport.initialize())

app.get(
    '/',
    passport.authenticate("google", {scope:["email", "profile"]}),            
    (req,res)=>{
    }
);

app.get("/callback"
          ,passport.authenticate("google", {scope: ["email", "profile"]}),
           (req,res)=>{
                return res.send("congrats");
});
app.listen(3000,() => console.log("app listening on port 3000!"));